<?php

/**
 * @file
 * Contains the administration pages for Back To Top.
 *
 */

function contactonline_setting_form($form,&$form_state){
  //Include Farbtastic color picker library and other necessary resources.
  drupal_add_library('system','farbtastic');
  drupal_add_js(drupal_get_path('module','contactonline').'/js/contactonline.admin.js');
  drupal_add_css(drupal_get_path('module','contactonline').'/css/contactonline.admin.css');
  //Display
  $contactonline = unserialize(variable_get('contactonline_settings'));
  $form['display'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display Setting'),
    '#collapsible' => TRUE,
  );
  $form['display']['display_in_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show in block'),
    '#default_value' => $contactonline['display_in_block'],
  );
  $form['display']['display_block_input'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="display_in_block"]' => array('checked' => FALSE),
      ),
    ),
  );
  //Get block from database if exist
  $block = db_query('SELECT region FROM {block} WHERE module=:module AND delta=:delta',array(':module' => 'contactonline','delta' => 'contactonline'))->fetchObject();
  $form['display']['display_block_input']['block_location'] = array(
    '#type' => 'select',
    '#title' => t('Block location'),
    '#options' => system_region_list(variable_get('theme_default','bartik'),REGIONS_VISIBLE),
    '#default_value' => $contactonline['block_location']?$contactonline['block_location']:'footer',
    '#description' => t('Select block location'),
  );
  $form['display']['display_in_content'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add to content body'),
    '#default_value' => $contactonline['display_in_content'],
  );
  $form['display']['float_in_page'] = array(
    '#type' => 'checkbox',
    '#title' => t('Roll in page'),
    '#default_value' => $contactonline['float_in_page'],
  );
  $form['display']['float_in_page_input'] = array(
    '#type' =>'container',
    '#states' => array(
      'invisible' => array(
        'input[name="float_in_page"]' => array(
          'checked' => FALSE
        ),
      ),
    ),
  );
  //add from 2.0
  $form['display']['float_in_page_input']['float_in_page_prevent_on_mobile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prevent on mobile and touch devices'),
    '#description' => t('Do you want to prevent Contact Online on touch devices?'),
    '#default_value' => $contactonline['float_in_page_prevent_on_mobile'] ? $contactonline['float_in_page_prevent_on_mobile'] : TRUE,
  );
  $form['display']['float_in_page_input']['float_in_page_prevent_in_admin'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prevent on administration pages and node edit'),
    '#description' => t('Do you want to prevent Contact Online admin pages?'),
    '#default_value' => $contactonline['float_in_page_prevent_in_admin'],
  );
  $form['display']['float_in_page_input']['float_in_page_prevent_in_front'] = array(
    '#type' => 'checkbox',
    '#title' => t('Prevent on front page'),
    '#description' => t('Do you want to prevent Contact Online on front page?'),
    '#default_value' => $contactonline['float_in_page_prevent_in_front']?$contactonline['float_in_page_prevent_in_front']:FALSE,
  );
  $form['display']['float_in_page_input']['float_in_page_button_place'] = array(
    '#title' => t('Placement'),
    '#description' => t('Where should the Contact Online button appear?'),
    '#type' => 'select',
    '#options' => array(
      1 => t('Bottom right'),
      2 => t('Bottom left'),
      3 => t('Bottom center'),
      4 => t('Top right'),
      5 => t('Top left'),
      6 => t('Top center'),
      7 => t('Mid right'),
      8 => t('Mid left'),
      9 => t('Mid center'),
    ),
    '#default_value' => $contactonline['float_in_page_button_place']?$contactonline['float_in_page_button_place']:3,
  );

  $form['display']['float_in_page_input']['float_in_page_button_type'] = array(
    '#type' => 'radios',
    '#title' => t('Do you want float in page to use a PNG-24 image or a Text/Css button?'),
    '#options' => array(
      'image' => t('Image (default)'),
      'text' => t('Text/Css')
    ),
    '#default_value' => $contactonline['float_in_page_button_type'],
  );

  // Wrap Text/Css button settings in a fieldset.
  $form['display']['float_in_page_input']['text_button'] = array(
    '#type' => 'container',
    '#title' => t('Text/Css button settings'),
    '#states' => array(
      'invisible' => array(
        'input[name="float_in_page_button_type"]' => array('value' => 'image'),
      ),
    ),
  );
  $form['display']['float_in_page_input']['text_button']['float_in_page_button_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Button text'),
    '#description' => t('Set the text of the float in page button'),
    '#default_value' => $contactonline['float_in_page_button_text'] ? $contactonline['float_in_page_button_text'] : "Contact Us",
    '#size' => 30,
    '#maxlength' => 30,
  );
/*  $form['display']['float_in_page_input']['text_button']['float_in_page_button_bg_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Background color'),
    '#description' => t('Button background color default #F7F7F7'),
    '#default_value' => $contactonline['float_in_page_button_bg_color']?$contactonline['float_in_page_button_bg_color']:'#F7F7F7',
    '#size' => 10,
    '#maxlength' => 7,
    '#suffix' => '<div class="color-field" id="float_in_page_button_bg_color"></div>',
  );
  $form['display']['float_in_page_input']['text_button']['float_in_page_button_border_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Border color'),
    '#description' => t('Border color default #CCCCCC'),
    '#default_value' => $contactonline['float_in_page_button_border_color']?$contactonline['float_in_page_button_border_color']:'#cccccc',
    '#size' => 10,
    '#maxlength' => 7,
    '#suffix' => '<div class="color-field" id="float_in_page_button_border_color"></div>',
  );
  $form['display']['float_in_page_input']['text_button']['float_in_page_button_hover_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Hover color'),
    '#description' => t('Hover color default #EEEEEE'),
    '#default_value' => $contactonline['float_in_page_button_hover_color']?$contactonline['float_in_page_button_hover_color']:'#EEEEEE',
    '#size' => 10,
    '#maxlength' => 7,
    '#suffix' => '<div class="color-field" id="float_in_page_button_hover_color"></div>',
  );
  $form['display']['float_in_page_input']['text_button']['float_in_page_button_text_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Text color'),
    '#description' => t('Text color default #333333'),
    '#default_value' => $contactonline['float_in_page_button_text_color'],
    '#size' => 10,
    '#maxlength' => 7,
    '#suffix' => '<div class="color-field" id="float_in_page_button_text_color"></div>',
  );
 */
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account Setting'),
    '#collapsible' => TRUE,
  );
  //Account settings 
  // Skype account setting
  $form['settings']['skype_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Skype'),
    '#default_value' => $contactonline['skype_enabled'],
  );
  $form['settings']['skype_input'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="skype_enabled"]' => array(
          'checked' => FALSE
        ),
      ),
    ),
  );
  $form['settings']['skype_input']['skype_accounts'] = array(
    '#type' => 'textfield',
    '#title' => t('Account'),
    '#size' => 50,
    '#description' => t('Skype account,accounts separated by spacebar.'),
    '#default_value' => $contactonline['skype_accounts'],
  );
  if($contactonline['display_in_block']){
    $form['settings']['skype_input']['display_in_block_skype_image_style'] = array(
      '#title' => t('Display in block skype image style'),
      '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_skype_online_images(),
      '#default_value' => $contactonline['display_in_block_skype_image_style'],
    );
  }
  if($contactonline['display_in_content']){
    $form['settings']['skype_input']['display_in_content_skype_image_style'] = array(
      '#title' => t('Display in content skype image style'),
      '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_skype_online_images(),
      '#default_value' => $contactonline['display_in_content_skype_image_style'],
    );
  }
  if($contactonline['float_in_page']){
    $form['settings']['skype_input']['float_in_page_skype_image_style'] = array(
      '#title' => t('Float in page skype image style'),
      '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_skype_online_images(),
      '#default_value' => $contactonline['float_in_page_skype_image_style'],
    );
  }
  //QQ account setting
  $form['settings']['qq_enabled'] =  array(
    '#type' => 'checkbox',
    '#title' => t('Enable QQ'),
    '#default_value' => $contactonline['qq_enabled'],
  );
  $form['settings']['qq_input'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="qq_enabled"]' => array(
          'checked' => FALSE
        ),
      ),
    ),
  );
  $form['settings']['qq_input']['qq_accounts'] = array(
    '#type' => 'textfield',
    '#title' => t('Account'),
    '#size' => 50,
    '#default_value' => $contactonline['qq_accounts'],
    '#description' => t('QQ account by tencent,Multiple accounts separated by spacebar,Failure in "QQ online status" official website, login before you can use this'),
  );
  if($contactonline['display_in_block']){
    $form['settings']['qq_input']['display_in_block_qq_image_style'] = array(
      '#title' => t('Display in block qq image style'),
      // '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_qq_online_images(),
      '#default_value' => $contactonline['display_in_block_qq_image_style'],
    );
  }
  if($contactonline['display_in_content']){
    $form['settings']['qq_input']['display_in_content_qq_image_style'] = array(
      '#title' => t('Display in content qq image style'),
      '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_qq_online_images(),
      '#default_value' => $contactonline['display_in_content_qq_image_style'],
    );
  }
  if($contactonline['float_in_page']){
    $form['settings']['qq_input']['float_in_page_qq_image_style'] = array(
      '#title' => t('Float in page qq image style'),
      '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_qq_online_images(),
      '#default_value' => $contactonline['float_in_page_qq_image_style'],
    );
  }
  //Aliwangwang account setting
  $form['settings']['aliwangwang_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Aliwangwang'),
    '#default_value' => $contactonline['aliwangwang_enabled'],
  );
  $form['settings']['aliwangwang_input'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="aliwangwang_enabled"]' => array(
          'checked' => FALSE
        ),
      ),
    ),
  );
  $form['settings']['aliwangwang_input']['aliwangwang_accounts'] = array(
    '#type' => 'textfield',
    '#title' => t('Account'),
    '#size' => 50,
    '#default_value' => $contactonline['aliwangwang_accounts'],
    '#description' => t('Aliwangwang account by alibaba,Multiple accountsseparated by spacebar'),
  );
  if($contactonline['display_in_block']){
    $form['settings']['aliwangwang_input']['display_in_block_aliwangwang_image_style'] = array(
      '#title' => t('Display in block qq image style'),
      // '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_aliwangwang_online_images(),
      '#default_value' => $contactonline['display_in_block_aliwangwang_image_style'],
    );
  }
  if($contactonline['display_in_content']){
    $form['settings']['aliwangwang_input']['display_in_content_aliwangwang_image_style'] = array(
      '#title' => t('Display in content aliwangwang image style'),
      '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_aliwangwang_online_images(),
      '#default_value' => $contactonline['display_in_content_aliwangwang_image_style'],
    );
  }
  if($contactonline['float_in_page']){
    $form['settings']['aliwangwang_input']['float_in_page_aliwangwang_image_style'] = array(
      '#title' => t('Float in page aliwangwang image style'),
      '#description' => t('Where should the Contact Online button appear?'),
      '#type' => 'radios',
      '#options' => get_aliwangwang_online_images(),
      '#default_value' => $contactonline['float_in_page_aliwangwang_image_style'],
    );
  }
  $form['settings']['email_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add E-mail'),
    '#default_value' => $contactonline['email_enabled'],
  );
  $form['settings']['email_input'] = array(
    '#type' => 'container',
    '#states' => array(
      'invisible' => array(
        'input[name="email_enabled"]' => array(
          'checked' => FALSE
        ),
      ),
    ),
  );
  $form['settings']['email_input']['email_accounts'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#size' => 50,
    '#default_value' => $contactonline['email_accounts'],
    '#description' => t('Multiple E-mail address separated by spacebar'),
  );
  //  variable_set('contactonline_settings',serialize($contactonline));
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Build and Save'),
  );
  return $form;
}

function contactonline_setting_form_submit($form,&$form_state){
  $contactonline_data = array(
    'display_in_block' => $form_state['values']['display_in_block'],
    'block_location' => $form_state['values']['block_location'],
    'display_in_content' => $form_state['values']['display_in_content'],
    'float_in_page' => $form_state['values']['float_in_page'],
    'float_in_page_prevent_on_mobile' => $form_state['values']['float_in_page_prevent_on_mobile'],
    'float_in_page_prevent_in_admin' => $form_state['values']['float_in_page_prevent_in_admin'],
    'float_in_page_prevent_in_front' => $form_state['values']['float_in_page_prevent_in_front'],
    'float_in_page_button_place' => $form_state['values']['float_in_page_button_place'],
    'float_in_page_button_type' => $form_state['values']['float_in_page_button_type'],
    'float_in_page_button_text' => !empty($form_state['values']['float_in_page_button_text'])?$form_state['values']['float_in_page_button_text']:'Contact Us',
    /*
    'float_in_page_button_bg_color' => !empty($form_state['values']['float_in_page_button_bg_color'])?$form_state['values']['float_in_page_button_bg_color']:'#F7F7F7',
    'float_in_page_button_border_color' => !empty($form_state['values']['float_in_page_button_border_color'])?$form_state['values']['float_in_page_button_border_color']:'#CCCCCC',
    'float_in_page_button_hover_color' => !empty($form_state['values']['float_in_page_button_hover_color'])?$form_state['values']['float_in_page_button_hover_color']:'#EEEEEE',
    'float_in_page_button_text_color' => !empty($form_state['values']['float_in_page_button_text_color'])?$form_state['values']['float_in_page_button_text_color']:'#333333',  
     */
    'skype_enabled' => $form_state['values']['skype_enabled'],
    'skype_accounts' => $form_state['values']['skype_accounts'],
    'display_in_block_skype_image_style' => !empty($form_state['values']['display_in_block_skype_image_style'])?$form_state['values']['display_in_block_skype_image_style']:'smallicon',
    'display_in_content_skype_image_style' => !empty($form_state['values']['display_in_content_skype_image_style'])?$form_state['values']['display_in_content_skype_image_style']:'smallicon',
    'float_in_page_skype_image_style' => !empty($form_state['values']['float_in_page_skype_image_style'])?$form_state['values']['float_in_page_skype_image_style']:'smallicon',    
    'qq_enabled' => $form_state['values']['qq_enabled'],
    'qq_accounts' => $form_state['values']['qq_accounts'],
    'display_in_block_qq_image_style' => !empty($form_state['values']['display_in_block_qq_image_style'])?$form_state['values']['display_in_block_qq_image_style']:1,
    'display_in_content_qq_image_style' => !empty($form_state['values']['display_in_content_qq_image_style'])?$form_state['values']['display_in_content_qq_image_style']:1,
    'float_in_page_qq_image_style' => !empty($form_state['values']['float_in_page_qq_image_style'])?$form_state['values']['float_in_page_qq_image_style']:1,    
    'aliwangwang_enabled' => $form_state['values']['aliwangwang_enabled'],
    'aliwangwang_accounts' => $form_state['values']['aliwangwang_accounts'],
    'display_in_block_aliwangwang_image_style' => !empty($form_state['values']['display_in_block_aliwangwang_image_style'])?$form_state['values']['display_in_block_aliwangwang_image_style']:1,
    'display_in_content_aliwangwang_image_style' => !empty($form_state['values']['display_in_content_aliwangwang_image_style'])?$form_state['values']['display_in_content_aliwangwang_image_style']:1,
    'float_in_page_aliwangwang_image_style' => !empty($form_state['values']['float_in_page_aliwangwang_image_style'])?$form_state['values']['float_in_page_aliwangwang_image_style']:1,    
    'email_enabled' => $form_state['values']['email_enabled'],
    'email_accounts' => $form_state['values']['email_accounts'],
  );

  //Processing contactonline block
  //Display block enable
  if($contactonline_data['display_in_block']){

    $block = db_query('SELECT * FROM {block} WHERE module = :module AND delta = :delta AND theme = :theme_default',array(':module' => 'contactonline',':delta' => 'contactonline',':theme_default' => variable_get('theme_default')))->fetchObject();
    //first time
    if(!empty($block)){
      $block_update = db_update('block')->fields(array('region' => $contactonline_data['block_location'],'status' => 1))->condition('delta','contactonline','=')->condition('theme',variable_get('theme_default'),'=')->execute(); 
    }else{
      $block_insert = db_insert('block')->fields(array('module' =>'contactonline','delta' => 'contactonline','theme' => variable_get('theme_default'),'region' => $contactonline_data['block_location'],'status' => 1))->execute();
    }
    get_codes_for_image_style('display_in_block',$contactonline_data);

  }else{
    $block_update = db_update('block')->fields(array('region' => -1,))->condition('delta','contactonline','=')->execute(); 
  }

  if($contactonline_data['display_in_content']){
    get_codes_for_image_style('display_in_content',$contactonline_data);
  }
  if($contactonline_data['float_in_page']){
    get_codes_for_image_style('float_in_page',$contactonline_data);
  }
  //Saving settings
  variable_set('contactonline_settings',serialize($contactonline_data));
}

/**
 *
 */
function get_codes_for_image_style($display,$contactonline_settings = array()){
  //Add contactonline codes to variable
  $contactonline_codes = "<div id='contactonline-".$display."'>";
  //TODO get skype account
  if($contactonline_settings['skype_enabled']){
    $skype_accounts = explode(" ",$contactonline_settings['skype_accounts']); 
    // $skype_accounts = explode(" ","one two three four"); 
    if(!empty($skype_accounts)){
      foreach($skype_accounts as $key=>$value){
        $v = trim($value);
        if(!($v == NULL)){
          $contactonline_codes .= "<a href='skype:".$value."?call' onclick='return skypeCheck();'> <img src='http://mystatus.skype.com/".$contactonline_settings[$display.'_skype_image_style']."/".$value."' style='border:none;' alt='call me!'/></a>" ;
        }
      }
    }
  }
  //TODO get qq account
  if($contactonline_settings['qq_enabled']){
    $qq_accounts = explode(" ",$contactonline_settings['qq_accounts']);
    if(!empty($qq_accounts)){
      foreach($qq_accounts as $key=>$value){
        $v = trim($value);
        if(!($v == NULL)){
          $contactonline_codes .= "<a target='_blank' href='http://wpa.qq.com/msgrd?v=3&uin=".$value."&site=qq&menu=yes'><img border='0' src='http://wpa.qq.com/pa?p=2:".$value.":".$contactonline_settings[$display.'_qq_image_style']."'/></a>";
        }
      }
    }
  }

  //TODO get aliwangwang account
  if($contactonline_settings['aliwangwang_enabled']){
    $aliwangwang_accounts = explode(" ",$contactonline_settings['aliwangwang_accounts']);
    if(!empty($aliwangwang_accounts)){
      foreach($aliwangwang_accounts as $key=>$value){
        $v = trim($value);
        if(!($v == NULL)){
          $contactonline_codes .= "<a href='http://amos.im.alisoft.com/msg.aw?v=2&uid=".$value."&site=enaliint&s=4'  target='_blank'><img src='http://web.im.alisoft.com/online.aw?v=2&uid=".$value."&site=enaliint&s=".$contactonline_settings[$display.'_aliwangwang_image_style']."' /></a>";
        }
      }
    }
  }
  //TODO get emails account
  if($contactonline_settings['email_enabled']){
    $emails_accounts = explode(" ",$contactonline_settings['email_accounts']);
    if(!empty($emails_accounts)){
      foreach($emails_accounts as $key=>$value){
        $v = trim($value);
        if(!($v == NULL)){
          $contactonline_codes .= "<a href='mailto:".$value."'><img src='/".drupal_get_path('module','contactonline')."/images/email.jpg' title='email to us' /></a>";
        }
      }
    }
  }
  $contactonline_codes .= "</div>";
  //Add generate code to variable contactonline_code
  variable_set('contactonline_'.$display.'_codes',$contactonline_codes);

}

function get_qq_online_images(){
  $images = array();
  $images[1] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_10.gif'/>";
  $images[2] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_20.gif'/>";
  $images[3] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_30.gif'/>";
  $images[4] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_40.gif'/>";
  $images[5] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_50.gif'/>";
  $images[6] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_60.gif'/>";
  $images[7] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_70.gif'/>";
  $images[10] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_100.gif'/>";
  $images[11] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_110.gif'/>";
  $images[12] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_120.gif'/>";
  $images[13] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_130.gif'/>";
  $images[14] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_140.gif'/>";
  $images[15] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_150.gif'/>";
  $images[16] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_160.gif'/>";
  $images[17] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_170.gif'/>";
  $images[20] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_old_200.gif'/>";
  $images[41] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_10.gif'/>";
  $images[42] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_20.gif'/>";
  $images[43] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_30.gif'/>";
  $images[44] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_40.gif'/>";
  $images[45] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_50.gif'/>";
  $images[46] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_60.gif'/>";
  $images[47] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_70.gif'/>";
  $images[49] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_90.gif'/>";
  $images[50] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_100.gif'/>";
  $images[51] = "<img border='0' src='hhttp://pub.idqqimg.com/qconn/wpa/button/button_110.gif'/>";
  $images[52] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_120.gif'/>";
  $images[53] = "<img border='0' src='http://pub.idqqimg.com/qconn/wpa/button/button_130.gif'/>";
  return $images;
}

function get_aliwangwang_online_images(){
  $aliwangwang_images = array();
  $aliwangwang_images[0] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/4/online.gif' />";		
  $aliwangwang_images[1] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/1/online.gif' />";		
  $aliwangwang_images[2] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/2/online.gif' />";	
  $aliwangwang_images[5] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/5/online.gif' />";		
  $aliwangwang_images[6] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/6/online.gif' />";		
  $aliwangwang_images[10] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/10/online.gif' />";		
  $aliwangwang_images[11] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/11/online.gif' />";		
  $aliwangwang_images[12] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/12/online.gif' />";		
  $aliwangwang_images[21] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/21/online.gif' />";		
  $aliwangwang_images[22] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/22/online.gif' />";		
  $aliwangwang_images[23] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/23/online.gif' />";		
  $aliwangwang_images[24] = "<img src='http://img.im.alisoft.com/actions/wbtx/alitalk/24/online.gif' />";		
  return $aliwangwang_images;
}

function get_skype_online_images(){
  $skype_images = array();
  $skype_images['smallclassic'] = "<img src='http://mystatus.skype.com/smallclassic/canzom' style='border:none;' alt='call me!'/>";
  $skype_images['smallicon'] = "<img src='http://mystatus.skype.com/smallicon/canzom' style='border:none;' alt='call me!'/>";
  $skype_images['mediumicon'] = "<img src='http://mystatus.skype.com/mediumicon/canzom' style='border:none;' alt='call me!'/>";
  $skype_images['bigclassic'] = "<img src='http://mystatus.skype.com/bigclassic/canzom' style='border:none;' alt='call me!'/>";
  $skype_images['balloon'] = "<img src='http://mystatus.skype.com/balloon/canzom' style='border:none;' alt='call me!'/>";
  return $skype_images;	
}
