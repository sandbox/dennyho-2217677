<?php
/**
 * *@file
 * Default theme implementation for rendering contactonline information
 *
 * Available variables:
 * - $contactonline: the name of the contactonline contactonline
 * */
?>
<div id="contactonline-content" >
  <div class="contactonline_title">Contact us: </div>
  <?php print $contactonline; ?>
</div>
