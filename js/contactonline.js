(function ($) {
 Drupal.behaviors.contactonlineDiv = {
attach: function(context) {
var exist= jQuery('#contactonline').length;
if(exist == 0) {
$("body", context).once(function() {
  $(this).append("<div id='contactonline'><span class='contactonline-link'>"+Drupal.t(Drupal.settings.contactonline.float_in_page_button_text)+"</span>"+Drupal.settings.contactonline.float_in_page_codes+"</div>");
  });
}
//$('#contactonline').fadeIn();

$('#contactonline',context).once(function(){
  var $block = $(this);
  $block.find('span.contactonline-link')
  .prepend('<span id="contactonline-toggle">[ + ]</span> ')
  .css('cursor','pointer')
  .toggle(function(){
    Drupal.contactonlineDivToggle($block,false);
    },
    function(){
    Drupal.contactonlineDivToggle($block,true);
    }
    );
  $block.find('#contaconline-float_in_page').hide();
  $block.show();
 });

/**
 * Collapse or uncollapse the contactonline form block.
 */
Drupal.contactonlineDivToggle = function($block,enable){
  $block.find('div#contactonline-float_in_page').slideToggle('medium');
  if(enable){
    $('#contactonline-toggle',$block).html('[ + ]');
  }
  else{
    $('#contactonline-toggle',$block).html('[ &minus; ]');
  }
};
}
};
})(jQuery);
