(function ($) {
    Drupal.behaviors.contactonline_admin = {
        attach: function(context) {
            $(document).ready(function() {
                $("#float_in_page_button_bg_color").farbtastic("#edit-float-in-page-button-bg-color");
                $("#float_in_page_button_border_color").farbtastic("#edit-float-in-page-button-border-color");
                $("#float_in_page_button_hover_color").farbtastic("#edit-float-in-page-button-hover-color");
                $("#float_in_page_button_text_color").farbtastic("#edit-float-in-page-button-text-color");
            });
        }
    };
})(jQuery);
